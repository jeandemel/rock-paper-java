import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        Game game = new Game();

        Scanner scanner = new Scanner(System.in);

        while(true) {
            System.out.println("Choose between rock, scissor or paper");
            String choice = scanner.nextLine();
            

            game.play(choice);

        }

    }
}
