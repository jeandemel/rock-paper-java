import java.util.Random;

public class Game {
    private String[] symbols = {"rock", "paper", "scissor"};


    public void play(String symbolChoice) {

        String adversaryChoice = this.symbols[new Random().nextInt(symbols.length)];

        int result = this.compareSymbols(symbolChoice, adversaryChoice);

        System.out.println("You chose "+symbolChoice+", computer chose "+adversaryChoice);
        if(result == 1) {
            System.out.println("You won");
        }
        if(result == 0) {
            System.out.println("everyone lose");
        }
        if(result == -1) {
            System.out.println("You lose");
        }
    }

    private int compareSymbols(String symbolA, String symbolB) {

        if((symbolA.equals("rock") && symbolB.equals( "scissor")) ||
        (symbolA.equals("paper") && symbolB.equals("rock")) ||
        (symbolA.equals("scissor") && symbolB.equals("paper"))) {

            return 1;
        }
        if(symbolA.equals(symbolB)) {
            return 0;
        }

        return -1;
        
    }


}
